MAJOR=0
MINOR=1
VERSION=$(MAJOR).$(MINOR)

#-------------------------
# Installation directories
prefix=/usr/local
exec_prefix=$(prefix)
bindir=$(exec_prefix)/bin

datarootdir=$(prefix)/share
datadir=$(datarootdir)

configdir=${HOME}/.config/satori

ifeq ($(TMPDIR),)
	tempdir=/tmp
else
	tempdir=$(TMPDIR)
endif

#-----------------------------------
# Paths for sources and object files
SRCDIR=src
OBJDIR=$(SRCDIR)/obj

#---------------
# Compiler and compiler flags
CC=g++
CFLAGS=-Wall -g
OBJ_CFLAGS=-c $(CFLAGS)
LIBSFML=-lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio -lsfml-network

.PHONY: test clean

test: satori
	./satori
 
satori: $(OBJDIR)/Sudoku.o $(OBJDIR)/Stage.o $(OBJDIR)/Game.o $(OBJDIR)/main.o
	$(CC) $(CFLAGS) $(OBJDIR)/Sudoku.o $(OBJDIR)/Stage.o $(OBJDIR)/Game.o $(OBJDIR)/main.o -o satori $(LIBSFML)

$(OBJDIR)/main.o: $(OBJDIR)/Game.o $(SRCDIR)/main.cpp
	$(CC) $(OBJ_CFLAGS) $(SRCDIR)/main.cpp -o $(OBJDIR)/main.o

$(OBJDIR)/Game.o: $(OBJDIR)/Stage.o $(OBJDIR)/Sudoku.o $(SRCDIR)/Game.cpp $(SRCDIR)/Game.h $(SRCDIR)/Config.h
	$(CC) $(OBJ_CFLAGS) $(SRCDIR)/Game.cpp -o $(OBJDIR)/Game.o

$(OBJDIR)/Sudoku.o: $(SRCDIR)/Sudoku.cpp $(SRCDIR)/Sudoku.h $(SRCDIR)/Config.h
	$(CC) $(OBJ_CFLAGS) $(SRCDIR)/Sudoku.cpp -o $(OBJDIR)/Sudoku.o

$(OBJDIR)/Stage.o: $(SRCDIR)/Stage.cpp $(SRCDIR)/Stage.h $(SRCDIR)/Config.h
	$(CC) $(OBJ_CFLAGS) $(SRCDIR)/Stage.cpp -o $(OBJDIR)/Stage.o

clean:
	rm -f satori $(OBJDIR)/*.o