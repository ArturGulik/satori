#include "Sudoku.h"
#include <iostream>

int randomInt(int a, int b){
    return rand()%b + a;
}

// PRIVATE
bool Sudoku::validateBoard(bool coloring=false){
   int columns[9][9], rows[9][9];
    for (int i=0;i<81;++i) {
        columns[i%9][i/9] = 0;
        rows[i%9][i/9] = 0;
    }
    // columns[x][n] answers the question:
    // How many n-1 numbers are located in column x?

    for(int x=0;x<9;++x){
        for(int y=0;y<9;++y){
            if (this->sudoku[x][y] == 0) continue;
            if (rows[this->sudoku[x][y]-1][y] > 0 && !coloring) return false;
            rows[this->sudoku[x][y]-1][y] += 1;
            if (columns[x][this->sudoku[x][y]-1] > 0 && !coloring) return false;
            columns[x][this->sudoku[x][y]-1] += 1;
        }
    }
    bool conflict = false;
    for(int x=0;x<9;++x){
        for(int y=0;y<9;++y){
            //if (columns)
            if (columns[x][this->sudoku[x][y]-1] > 1) {
                conflict = true;
                gridRects[x][y].setPosition(sf::Vector2f(
                (x)*(gridRectSize+config.board.grid.thickness)+config.board.position.x,
                (y)*(gridRectSize+config.board.grid.thickness)+config.board.position.y));
                gridRects[x][y].setFillColor(sf::Color(255,0,0,255));
                windowP->draw(gridRects[x][y]);
            }
        }
    }
    return conflict;
}
void Sudoku::initBoard(){
    this->status = 0;
    this->cursor.x = 4;
    this->cursor.y = 4;

    board.setSize(this->config.board.size);
    board.setFillColor(this->config.board.color);
        //board.setOutlineColor(this->config.board.outline.color);
        //board.setOutlineThickness(this->config.board.outline.thickness);
    board.setPosition(this->config.board.position);
    board.setOutlineColor(sf::Color::Red);
    board.setOutlineThickness(1.f);

    this->gridRectSize = (this->config.board.size.x-8*this->config.board.grid.thickness)/9;

    for(int i=0; i<81;++i) {
        this->sudoku[i%9][i/9] = 0;
        this->gridRects[i%9][i/9].setFillColor(sf::Color(0,0,0,0));
        this->gridRects[i%9][i/9].setPosition(sf::Vector2f(
            (i%9)*(gridRectSize+config.board.grid.thickness)+config.board.position.x,
            (i/9)*(gridRectSize+config.board.grid.thickness)+config.board.position.y));
        this->gridRects[i%9][i/9].setSize(sf::Vector2f(this->gridRectSize,this->gridRectSize));
        this->gridRects[i%9][i/9].setOutlineColor(this->config.board.grid.color);
        this->gridRects[i%9][i/9].setOutlineThickness(this->config.board.grid.thickness);
    }

    this->zoneRectSize = (this->config.board.size.x-2*this->config.board.grid.zoneThickness)/3;

    sf::RectangleShape zoneRect;
    zoneRect.setSize(sf::Vector2f(this->zoneRectSize,this->zoneRectSize));
    zoneRect.setFillColor(sf::Color(0,0,0,0));
    zoneRect.setOutlineColor(this->config.board.grid.zoneColor);
    zoneRect.setOutlineThickness(this->config.board.grid.zoneThickness);
    this->zoneRect = zoneRect;    

    // RANDOMIZE BOARD
    int rand = randomInt(5, 15);
    for(int i=0;i<rand;++i)
        this->sudoku[randomInt(0, 8)][randomInt(0, 8)] = randomInt(1, 9);
}


//PUBLIC
void Sudoku::manageEvent(sf::Event event){
    //std::cout<<event.key.code<<"\n";
    if (event.key.code < 36 && event.key.code > 25){
        this->sudoku[cursor.x][cursor.y] = event.key.code-26;
        return;
    }
    switch (event.key.code){
        case sf::Keyboard::N:
            this->status = 1; // tell the game engine, that we need to swap stages
        break;
        case sf::Keyboard::Left:
        case sf::Keyboard::H:
            if (this->cursor.x > 0) this->cursor.x-=1;
        break;
        case sf::Keyboard::Down:
        case sf::Keyboard::J:
            if (this->cursor.y < 8) this->cursor.y += 1;
        break;
        case sf::Keyboard::Up:
        case sf::Keyboard::K:
            if (this->cursor.y > 0) this->cursor.y-=1;
        break;
        case sf::Keyboard::Right:
        case sf::Keyboard::L:
            if (this->cursor.x < 8) this->cursor.x += 1;
        break;
        default: break;
    }
}
void Sudoku::render(){
    windowP->clear(config.menu.color);
    windowP->draw(board);

    for(int i=0; i<81;++i){
        if (cursor.x == (i%9) && cursor.y == (i/9)){
            if (validateBoard())
                gridRects[i%9][i/9].setFillColor(config.board.cursor.color);
            else
                gridRects[i%9][i/9].setFillColor(sf::Color(255,0,0,255));
        }
        else if (cursor.x == (i%9) || cursor.y == (i/9))
            gridRects[i%9][i/9].setFillColor(config.board.cursor.tail);

        windowP->draw(gridRects[i%9][i/9]);
        gridRects[i%9][i/9].setFillColor(sf::Color(0,0,0,0)); 
        if (sudoku[i%9][i/9] == 0) continue;
        number.setString(std::to_string(sudoku[i%9][i/9]));
        number.setPosition(sf::Vector2f(
        (i%9 +0.25f)*(gridRectSize+config.board.grid.thickness)+config.board.position.x,
        (i/9)*(gridRectSize+config.board.grid.thickness)+config.board.position.y));
        windowP->draw(number); 
    }
    for(int i=0; i<9;++i){
        zoneRect.setPosition(sf::Vector2f(
        (i%3)*(zoneRectSize+config.board.grid.zoneThickness)+config.board.position.x,
        (i/3)*(zoneRectSize+config.board.grid.zoneThickness)+config.board.position.y));  
        windowP->draw(zoneRect);
    }
}

Sudoku::Sudoku(sf::RenderWindow* windowPointer, Config givenConfig){
    this->config = givenConfig;
    this->initBoard();
    this->windowP = windowPointer;

    this->number.setFont(this->config.board.grid.number.font);
    this->number.setCharacterSize(this->config.board.grid.number.size);
    this->number.setFillColor(this->config.board.grid.number.color);
}

Sudoku::~Sudoku(){

}