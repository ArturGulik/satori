#include "Game.h"

//PRIVATE

bool Game::initConfig(){
    if (!this->config.menu.button.text.font.loadFromFile("fonts/WeretigerRegular.ttf")){
        printf("Error while loading the font file\n");
        return false;
    }
    if (!this->config.menu.title.font.loadFromFile("fonts/WeretigerRegular.ttf")){
        printf("Error while loading the font file\n");
        return false;
    }
    if (!this->config.board.grid.number.font.loadFromFile("fonts/WeretigerRegular.ttf")){
        printf("Error while loading the font file\n");
        return false;
    }
    this->config.menu.title.size = 72;
    this->config.menu.title.color = sf::Color(255, 255, 255, 255);

    this->config.menu.color = sf::Color(30, 30, 25, 255);
    this->config.menu.button.color = sf::Color(100, 100, 95, 255);
    this->config.menu.button.outline.color = sf::Color(0, 0, 0, 255);
    this->config.menu.button.outline.thickness = 3.f;
    this->config.menu.button.size = sf::Vector2f(250.f,100.f);

    this->config.menu.button.text.size = 26;
    this->config.menu.button.text.color = sf::Color(0, 0, 0, 255);
    
    this->config.board.bgColor = sf::Color(sf::Color(50, 50, 45, 255));
    this->config.board.color = sf::Color(sf::Color(90,90,90, 255));
    this->config.board.size = sf::Vector2f(422.f, 422.f);

    this->config.board.grid.color = sf::Color(205, 205, 205, 255);
    this->config.board.grid.thickness = 1.f;

    this->config.board.grid.zoneColor = sf::Color(255, 255, 255, 255);
    this->config.board.grid.zoneThickness = 2.f;
    
    this->config.board.grid.number.size = 35;
    this->config.board.grid.number.color = sf::Color(255, 255, 255, 255);
    
    this->config.board.cursor.color = sf::Color(60, 60, 115, 255);
    this->config.board.cursor.tail = sf::Color(100, 100, 155, 115);


    this->config.board.position = sf::Vector2f(29.f, 50.f);
    return true;
}

bool Game::initVariables(){
    // Initializes the class's variables.
    // Returns true on success and false on failure

    this->stage = 0; // MENU
    this->windowP = nullptr;
    this->menuP = nullptr;
    this->sudokuP = nullptr;
    this->windowSize.width = 480;
    this->windowSize.height = 800;
    this->windowName = "Satori";

    //CONFIG
    return this->initConfig();
}

void Game::initWindow(){
    // Has to be called AFTER initVariables()
    this->windowP = new sf::RenderWindow(this->windowSize, this->windowName);
    this->windowP->setFramerateLimit(60);

    // Creating menu objects
    sf::RectangleShape newGame;
    newGame.setSize(this->config.menu.button.size);
    newGame.setFillColor(this->config.menu.button.color);
    newGame.setOutlineColor(this->config.menu.button.outline.color);
    newGame.setOutlineThickness(this->config.menu.button.outline.thickness);
    newGame.setPosition(sf::Vector2f(100.f,150.f));

    sf::RectangleShape* rectangles[] = {&newGame};


    sf::Text gameNameText;
    gameNameText.setFont(this->config.menu.title.font);
    gameNameText.setString("SATORI");
    gameNameText.setCharacterSize(this->config.menu.title.size);
    gameNameText.setFillColor(this->config.menu.title.color);
    gameNameText.setPosition(sf::Vector2f(120.f, 20.f));

    sf::Text newGameText;
    newGameText.setFont(this->config.menu.button.text.font);
    newGameText.setString("Nowa gra");
    newGameText.setCharacterSize(this->config.menu.button.text.size);
    newGameText.setFillColor(this->config.menu.button.text.color);
    newGameText.setPosition(sf::Vector2f(120.f, 170.f));

    sf::Text* texts[] = {&gameNameText, &newGameText};
    this->menuP = new Stage(this->windowP, this->config,
        rectangles, 1, texts, 2);
}

void Game::manageEvents(){
    if (this->windowP->pollEvent(this->event)){
        switch (this->event.type){
            case sf::Event::Closed:
                this->windowP->close();
            break;
            case sf::Event::KeyPressed:
                switch(this->stage){
                    case 0:
                        this->menuP->manageEvent(this->event);
                    break;
                    case 1:
                        this->sudokuP->manageEvent(this->event);
                    break;
                    default: break;
                }
            break;
            default: break;
        }
    }
}

void Game::update(){
    this->manageEvents();
    if (this->stage == 0 && this->menuP->status == 1){
        this->sudokuP = new Sudoku(this->windowP, this->config);
        this->stage = 1;
    }
    else if (this->stage == 1 && this->sudokuP->status == 1){
        delete this->sudokuP;
        this->menuP->status = 0;
        this->stage = 0;
    }
        
}

void Game::render(){
    switch(this->stage){
        case 0:
            this->menuP->render();
        break;
        case 1:
            this->sudokuP->render();
        break;
        default:
            printf("Unknown stage!");
        break;
    }
    this->windowP->display();
}


//PUBLIC
void Game::loop(){
    if (!this->good) return;
    while(this->windowP->isOpen()){
        this->update();
        this->render();
        //auto [x,y] = sf::Mouse::getPosition(*this->windowP);
        //std::cout<<"Mouse Position: " << x << " " << y << "\r";
    }
}

Game::Game(){
    srand(time(NULL));
    this->good = this->initVariables();
    if (!this->good) return;
    this->initWindow();
}

Game::~Game(){
    delete this->windowP;
    delete this->menuP;
    delete this->sudokuP;
}