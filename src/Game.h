#ifndef GAME_H
#define GAME_H

#include <SFML/Graphics.hpp>

#include "Config.h"
#include "Stage.h"
#include "Sudoku.h"

class Game{
/* This class serves as the Game engine.
It's responsible for updating the state of the application
as well as rendering graphics to the game window or outputting
information via the standard output */
private:
    sf::RenderWindow* windowP;
    // event stores the event that has been or is being processed
    sf::Event event;
    sf::VideoMode windowSize;
    std::string windowName;
    Config config;
    int stage; // current stage indicator
    Stage* menuP;
    Sudoku* sudokuP;

    bool initConfig();
    bool initVariables();
    void initWindow();

    bool good;
    void manageEvents();
    void update();
    void render();
public: 
    void loop();
    //Contructor & Destructor
    Game();
    virtual ~Game();
};

#endif