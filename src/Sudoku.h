#ifndef SUDOKU_H
#define SUDOKU_H

#include "Config.h"
#include <SFML/Graphics.hpp>

class Sudoku{
    private:
    sf::RenderWindow* windowP;
    Config config;
    void initBoard();
    sf::RectangleShape board;
    sf::RectangleShape zoneRect;
    float zoneRectSize;
    float gridRectSize;
    int sudoku[9][9];
    sf::Text number;
    sf::Vector2i cursor;
    bool validateBoard(bool);
    std::vector<sf::Vector2i> conflicts;
    sf::RectangleShape gridRects[9][9];

    public:
    int status;
    // General Functions
    void manageEvent(sf::Event);
    //void click();
    void render();
    //Contructor & Destructor
    Sudoku(sf::RenderWindow*, Config);
    virtual ~Sudoku();
};

#endif