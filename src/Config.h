#ifndef CONFIG_H
#define CONFIG_H
#include <SFML/Graphics.hpp>

typedef struct Outline Outline;
struct Outline{
    sf::Color color;
    float thickness;
};

typedef struct Text Text;
struct Text{
    sf::Color color;
    unsigned int size;
    sf::Font font;
};

typedef struct Button Button;
struct Button{
    sf::Color color;
    sf::Vector2f size;
    Text text;
    Outline outline;
};

typedef struct Menu Menu;
struct Menu{
    Text title;
    sf::Color color;
    Button button;
};

typedef struct Grid Grid;
struct Grid{
    sf::Color color;
    sf::Color zoneColor;
    float thickness;
    float zoneThickness;
    Text number;
};

typedef struct Cursor Cursor;
struct Cursor{
    sf::Color color;
    sf::Color tail;
};

typedef struct Board Board;
struct Board{
    Grid grid;
    Cursor cursor;
    sf::Color color;
    sf::Color bgColor;
    sf::Vector2f size;
    sf::Vector2f position;
};

typedef struct Config Config;
struct Config{
    Menu menu;
    Board board;
};
#endif