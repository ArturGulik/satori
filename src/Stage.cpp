#include "Stage.h"

//PRIVATE
void Stage::initVariables(){
    this->windowP = nullptr;
    this->rectangleCount = 0;
    this->textCount = 0;
    this->status = 0;
}

//PUBLIC
void Stage::manageEvent(sf::Event event){
    switch (event.key.code){
        case sf::Keyboard::N:
            this->status = 1; // tell the game engine that we need to swap stages
        break;
        default: break;
    }
}

void Stage::render(){
    this->windowP->clear(this->config.menu.color);
    for (int i=0;i < this->rectangleCount;++i)
        this->windowP->draw(this->rectangles[i]);
    for (int i=0;i < this->textCount;++i)
        this->windowP->draw(this->texts[i]);
}

Stage::Stage(sf::RenderWindow* windowPointer, Config config,
             sf::RectangleShape* rectanglePointers[], int rCount,
             sf::Text* textPointers[], int tCount){
    this->initVariables();
    this->windowP = windowPointer;
    this->config = config;
    this->rectangleCount = rCount;
    for (int i=0;i < rCount;++i)
        this->rectangles.push_back(*rectanglePointers[i]);
    this->textCount = tCount;
    for (int i=0; i<tCount;++i)
        this->texts.push_back(*textPointers[i]);
    
}

Stage::~Stage(){
    
}